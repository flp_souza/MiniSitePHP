<html>
<head>
    <title>MeuSite.com</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/index.css">
</head>
<body>
    <?php
        include_once("topo.php");
        include_once("menu.php");

        if(empty($_SERVER["QUERY_STRING"])){
            $var = "conteudo.php";
            include_once("$var");
        }else{
            $pg = $_GET['pg'];
            include_once("$pg.php");
        }

        include_once("rodape.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="bootstrap/js/bootstrap.js">
</body>
</html>
