<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div class="container">
        <h3>Fale Conosco</h3>
        <form method="post">
          Nome: <input class="form-control" type="text" name="nome"/><br>
          E-mail: <input class="form-control" type="email" name="email"/><br>
          Telefone: <input class="form-control" type="tel" name="telefone"/><br>
          Assunto: <input class="form-control" type="text" name="assunto"/><br>
          Mensagem:<textarea class="form-control" name="mensagem" rows="5" cols="10"></textarea>
          <br><br><button class="btn btn-info">Enviar</button>
        </form>
    </div>
  </body>
</html>
